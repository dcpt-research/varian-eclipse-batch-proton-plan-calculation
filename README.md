## A script to generate and calculate proton plans

This project is primarily meant for assesing HU accuracy (and/or density changes)
of CBCT. By optimising a simple beam, iterating over several angles, on a plan CT
and recalculating on CBCT.

For phantoms, the results should ideally be the same,
thus it would be a measure of accuracy.

For patients, the results would be influenced by anatomical changes,
thus it would be a measure of robustness.


## Building:

# Requirements:

* Varian Eclipse
  - The API is included, but it will only run on a machine with Eclipse installed.
* On Linux: docker
* On Windows: Visual Studio

# Windows

You'll want to use Visual Studio:
 - Open the solution (the .sln file)
 - Set the configuration to either Debug or Release
 - Build the project
 - Press the play icon
 - and "It just works" -Todd Howard

# Linux and other Unix(-like) platforms

This probably wont be useful for anything but debugging, as Varian Eclipse doesn't support Linux, AFAIK.

The build process is simple and Docker based:
```
docker build -t myapp .

docker run myapp
```

# Usage of CI Docker image:

This repository uses GitLab CI to generate a docker image,
which can be found in "Packages" -> "Container registry" (in the menu to the left)

It doens't work, yet, but eventually may be possible to use with:
```
docker pull registry.gitlab.com/dcpt-research/varian-eclipse-batch-proton-plan-calculation:latest

docker run varian-eclipse-batch-proton-plan-calculation:latest
```
