FROM mono:latest

MAINTAINER Andreas G. Andersen <andreasga22@gmail.com>

RUN mono --version

RUN mkdir -p /usr/src/app/source /usr/src/app/build
WORKDIR /usr/src/app/source

COPY . /usr/src/app/source
RUN nuget restore -Noninteractive

RUN msbuild Proton_batch_calc.sln /property:Configuration=Release /property:OutDir=/usr/src/app/build/
WORKDIR /usr/src/app/build

CMD [ "mono", "./Proton_batch_calc.exe" ]
